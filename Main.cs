﻿using GTA;
using GTA.Native;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace GiveWantedWeapons
{
    public class GiveWantedWeapons : Script
    {
        private const string INI_SETTINGS_SECTION_STRING = "SETTINGS", INI_GIVE_ON_START = "GIVE_WEAPONS_ON_STARTUP", INI_GIVE_AFTER_SWITCH = "GIVE_WEAPONS_AFTER_SWITCH_CHARACTER", INI_GIVE_START_MISSION = "GIVE_WEAPONS_WHEN_START_MISSION", INI_GIVE_AFTER_MISSION = "GIVE_WEAPONS_AFTER_COMPLETE_MISSION", INI_HOLD_KEY_STRING = "HOLD_KEY_GIVE_WEAPONS", INI_PRESS_KEY_STRING = "PRESS_KEY_GIVE_WEAPONS", INI_WEAPONS_SECTION_STRING = "WEAPONS", INI_WEAPON_PREFIX = "WEAPON_";

        private Keys holdKey, pressKey;
        private bool giveWeaponsOnStart, giveWeaponsWhenSwitchChar, giveWeaponsStartMission, giveWeaponsAfterMission;
        private List<WeaponHash> wantedWeapons = new List<WeaponHash>();
        private bool switchedChar = false;
        private bool wasInMission = false;
        private int lastNumMissionsCompleted = -1;

        public GiveWantedWeapons()
        {
            string settingsFileName = Filename.Substring(0, Filename.LastIndexOf(".")) + ".ini";

            if (!File.Exists(settingsFileName))
            {
                UI.ShowSubtitle("INI file does not exist.", 5000);
            }
            else
            {
                Mission.Initialize();

                holdKey = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_HOLD_KEY_STRING, Keys.None);
                pressKey = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_PRESS_KEY_STRING, Keys.PageUp);
                giveWeaponsOnStart = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_GIVE_ON_START, true);
                giveWeaponsWhenSwitchChar = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_GIVE_AFTER_SWITCH, true);
                giveWeaponsStartMission = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_GIVE_START_MISSION, true);
                giveWeaponsAfterMission = Settings.GetValue(INI_SETTINGS_SECTION_STRING, INI_GIVE_AFTER_MISSION, true);

                for (int i = 1; i < 150; i++)
                {
                    WeaponHash temp = Settings.GetValue<WeaponHash>(INI_WEAPONS_SECTION_STRING, INI_WEAPON_PREFIX + i.ToString(), 0);

                    if (temp != 0 && !wantedWeapons.Contains(temp))
                    {
                        wantedWeapons.Add(temp);
                    }
                }

                if (wantedWeapons.Count > 0)
                {
                    if (pressKey != Keys.None) KeyUp += GiveWantedWeapons_KeyUp;

                    Interval = 200;
                    Tick += GiveWantedWeapons_Tick;

                    lastNumMissionsCompleted = GetNumbersMissionsCompleted();
                }
                else
                {
                    UI.ShowSubtitle("No weapons found in INI file.", 5000);
                }
            }
        }

        private int GetNumbersMissionsCompleted()
        {
            OutputArgument outArg = new OutputArgument();
            Function.Call<int>(Hash.STAT_GET_INT, Game.GenerateHash("num_missions_completed"), outArg, -1);
            return outArg.GetResult<int>();
        }

        private void GiveWeapons(Ped ped)
        {
            for (int i = 0; i < wantedWeapons.Count; i++)
            {
                if (ped.Weapons.HasWeapon(wantedWeapons[i]))
                {
                    ped.Weapons[wantedWeapons[i]].Ammo = ped.Weapons[wantedWeapons[i]].MaxAmmo;
                }
                else
                {
                    ped.Weapons.Give(wantedWeapons[i], 1, false, true);
                    ped.Weapons[wantedWeapons[i]].Ammo = ped.Weapons[wantedWeapons[i]].MaxAmmo;
                }
            }
        }

        private void GiveWantedWeapons_Tick(object sender, System.EventArgs e)
        {
            Player plr;
            Ped playerPed;
            if (Game.IsLoading || Game.IsScreenFadedOut || Function.Call<bool>(Hash.IS_PLAYER_IN_CUTSCENE, (plr = Game.Player)) || !(playerPed = plr.Character).Exists() || !playerPed.IsAlive) return;
            
            if (giveWeaponsWhenSwitchChar)
            {
                if (Function.Call<bool>(Hash.IS_PLAYER_SWITCH_IN_PROGRESS)) switchedChar = true;
                else if (switchedChar)
                {
                    GiveWeapons(playerPed);
                    switchedChar = false;
                }
            }

            if (giveWeaponsStartMission)
            {
                if (Game.MissionFlag && !wasInMission)
                {
                    GiveWeapons(playerPed);
                }

                wasInMission = Game.MissionFlag;
            }

            if (giveWeaponsAfterMission)
            {
                if (Mission.DidMissionEnd)
                {
                    GiveWeapons(playerPed);
                }
            }

            if (giveWeaponsOnStart)
            {
                GiveWeapons(playerPed);
                giveWeaponsOnStart = false;
            }
        }

        private void GiveWantedWeapons_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == pressKey && (holdKey == Keys.None || Game.IsKeyPressed(holdKey)))
            {
                Ped playerPed;
                if (Game.IsLoading || Game.IsScreenFadedOut || !(playerPed = Game.Player.Character).Exists() || !playerPed.IsAlive) return;

                GiveWeapons(playerPed);

                UI.ShowSubtitle("Gave wanted weapons to player");
            }
        }
    }
}
